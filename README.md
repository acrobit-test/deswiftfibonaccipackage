# DESwiftFibonacciPackage

This is the package used for distribution of the DESwiftFibonacci SDK.

Add this to your project using Swift Package Manager and import the `DESwiftFibonacci` module in the file you want to call the fibonacci calculation.

## Example

```
Import DESwiftFibonacci

let fibonacci = DEFibonacci()
fibonaccio.calculate(usingNumber: 1) { result in
    print("Result of my calculation: \(result)")
}
```